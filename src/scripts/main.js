

/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';
//<script src="/path/to/web3.min.js"></script>

/*
$.getScript("truffle-contract.js", function(){
   alert("Сценарий загружен, но необязательно выполнен.");
}); */

/*
var noprovider=false;
App = {
  web3Provider: null,
  contracts: {},

init: function() {}
}


$.fn.popup = function(petId,petPrice) { 	//функция для открытия всплывающего окна:
			this.css('position', 'absolute').fadeIn();	//задаем абсолютное позиционирование и эффект открытия
			//махинации с положением сверху:учитывается высота самого блока, экрана и позиции на странице:
			this.css('top', ($(window).height() - this.height()) / 2 + $(window).scrollTop() + 'px');
			//слева задается отступ, учитывается ширина самого блока и половина ширины экрана
			this.css('left', ($(window).width() - this.width()) / 2  + 'px');
			//открываем тень с эффектом:
			$('.backpopup').fadeIn();
			//установим в скрытых полях
			document.getElementById("petId").value = petId;
			document.getElementById("price").value = petPrice;
			
		}

*/




// Shortcuts to DOM Elements.
var messageForm = document.getElementById('message-form');
var messageInput = document.getElementById('new-post-message');
var titleInput = document.getElementById('new-post-title');
var signInButton = document.getElementById('sign-in-button');
var signOutButton = document.getElementById('sign-out-button');
var splashPage = document.getElementById('page-splash');
var addPost = document.getElementById('add-post');
var addButton = document.getElementById('add');
var recentPostsSection = document.getElementById('recent-posts-list');
var userPostsSection = document.getElementById('user-posts-list');
var topUserPostsSection = document.getElementById('top-user-posts-list');
var recentMenuButton = document.getElementById('menu-recent');
var myPostsMenuButton = document.getElementById('menu-my-posts');
var myTopPostsMenuButton = document.getElementById('menu-my-top-posts');
var listeningFirebaseRefs = [];
var noprovider = false;

function makeBet() {
    // const Web3 = require('web3');
    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
    // const contract = require('truffle-contract');

    console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    //console.log(contract);


    //const truffleContract = require('truffle-blockchain-utils');


    //$(function () {  // equivalent to $(document).ready(...)
    if (typeof(web3) === "undefined") {
        console.log("her5");
        error("Unable to find web3. " +
            "Please run MetaMask (or something else that injects web3).");
    } else {
        //log("Found injected web3.");
        console.log("her6 web3good");
        web3 = new Web3(window.web3.currentProvider);

    }
    //});

    if (web3.version.network != 4) {
        console.log("her11");
        noprovider = true;
    } else {
        console.log("her11-2");
        //log("Connected to the Ropsten test network.");
    }

    //var address = "0xb94b8fa98c930f5998fc5fc9d13cf51e3fa3de3c";
    //var abi = [{"constant": false,"inputs": [],"name": "auctionEnd","outputs": [],"payable": false,"stateMutability": "nonpayable","type": "function"},{"constant": false,"inputs": [],"name": "bid","outputs": [],"payable": true,"stateMutability": "payable","type": "function"},{"constant": false,"inputs": [],"name": "withdraw","outputs": [{"name": "","type": "bool"}],"payable": false,"stateMutability": "nonpayable","type": "function"},{"inputs": [{"name": "_biddingTime","type": "uint256"},{"name": "_beneficiary","type": "address"}],"payable": false,"stateMutability": "nonpayable","type": "constructor"},{"anonymous": false,"inputs": [{"indexed": false,"name": "bidder","type": "address"},{"indexed": false,"name": "amount","type": "uint256"}],"name": "HighestBidIncreased","type": "event"},{"anonymous": false,"inputs": [{"indexed": false,"name": "winner","type": "address"},{"indexed": false,"name": "amount","type": "uint256"}],"name": "AuctionEnded","type": "event"},{"constant": true,"inputs": [],"name": "beneficiary","outputs": [{"name": "","type": "address"}],"payable": false,"stateMutability": "view","type": "function"},{"constant": true,"inputs": [],"name": "highestBid","outputs": [{"name": "","type": "uint256"}],"payable": false,"stateMutability": "view","type": "function"},{"constant": true,"inputs": [],"name": "highestBidder","outputs": [{"name": "","type": "address"}],"payable": false,"stateMutability": "view","type": "function"}];
    var betPrice = 100;
    //counter = web3.eth.contract(abi).at(address);
    var counter = 0;
    //var contract;
    //var contract;



    //import Web3 from './web3.js';
    //import {default as contract} from 'truffle-contract'

    //import contract_artifacts from 'SimpleAuction.json'

    //contract_artifacts="";




    //contract = contract(contract_artifacts);
    //contract.setProvider(web3);

    $.getJSON('SimpleAuction.json', function(data) {
		//$.getJSON('Adoption.json', function(data) {
        // Get the necessary contract artifact file and instantiate it with truffle-contract

        if (!noprovider) {

            var AdoptionArtifact = data;
            var contract = TruffleContract(AdoptionArtifact);
            console.log("her12");
            console.log("data:",data, "contract:", contract);
            console.log("her12");
            // Set the provider for our contract
			console.log('setProv', contract.setProvider)
            try { contract.setProvider(web3)}  catch (e) {console.log(e)}

        }
		
		web3.eth.getAccounts(function(error, accounts) {
			if (error) {
				//console.log(error);
				alert("her1");
			}

			var account = accounts[0];
			alert("her2");



			contract.deployed().then(function(instance) {
				//adoptionInstance = instance;
				//!!!передать сюда postId
				//alert("her3");
				console.log('instance', instance);
				return instance.bid(postId, {
					from: account,
					value: web3.toWei(petPrice, "ether"),
					gas: 4600000,
					to: "0x0f413c5ab1ee67f1ea87eb8d2b5bbda93b0e12ca"
				});

			}).then(function(result) {
				console.log('result', result);
				console.log("we made bet");
				alert("her4");
				return 1;
			}).catch(function(err) {
				alert("her5");
				console.log(err.message);
			});

			/*
	   contract
		  .at("0x57cbc7b3330e9bc0a13239fd86e2dcab3ae1eec7")
		  .then(instance =>{
			  her("55");
			instance.totalSupply({
			  from: this.account
			})
			.then(value =>{
			  console.log("Total Supply:",this.web3.fromWei(value, "ether").toString(10));
			})
			.catch(e => {
			  her("55-2");
			  console.log(e);
			});
		  })
	  */

		});
    });


    



    //web3.eth.contract(abi).at(address)(function(counter) {

    /*
    counter.bid.call(function (err, result) {
      if (err) {
        //return error(err);
    	console.log(error(err));
      } else {
        //log("getCount call executed successfully.");
      }

      // Use the function's return value
    });
    */


    /*
    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      
      counter.bid({from: account, value: web3.toWei(betPrice,"ether"), gas: 4600000, to: address})(function(error, result){
       if(!error)
           console.log(JSON.stringify(result));
       else
           console.error(error);
    }) */

    /*не раб
  
  counter.bid({from: account, value: web3.toWei(betPrice,"ether"), gas: 4600000, to: address})(function (err, hash) {
  if (err) {
	console.log(error(err));
    return error(err);
  }

  waitForReceipt(hash, function () {
    console.log("Transaction succeeded.");
  });
  });
  */

    /*
  
  counter.deployed().then(function(instance) {
  //App.contracts.Adoption.deployed().then(function(instance) {
	  
  //adoptionInstance = instance;
	
	//return adoptionInstance.adopt(petId, {from: account, value: web3.toWei(petPrice,"ether"), gas: 4600000, to: "0x7c5522ac06d0721b3b31b37b63078bcc719c74aa"});
	return counter.bid({from: account, value: web3.toWei(betPrice,"ether"), gas: 4600000, to: address});
  
  }).then(function(result) {
	
    return 1;
  }).catch(function(err) {
    console.log(err.message);
  });
  */


    //});


    //})

    //var account = accounts[0];
    //counter.bid({from: account, value: web3.toWei(betPrice,"ether"), gas: 4600000, to: address});

    /*
    counter.bid.sendTransaction(function (err, hash) {
      if (err) {
    	console.log(error(err));
        return error(err);
      }

      waitForReceipt(hash, function () {
        console.log("Transaction succeeded.");
      });
    });
    */

    return true;
}

/**
 * Saves a new post to the Firebase DB.
 */
// [START write_fan_out]
function writeNewPost(uid, username, picture, title, body) {
    // A post entry.
    var postData = {
        author: username,
        uid: uid,
        body: body,
        title: title,
        starCount: 0,
        authorPic: picture
    };

    // Get a key for a new Post.
    var newPostKey = firebase.database().ref().child('posts2').push().key;

    // Write the new post's data simultaneously in the posts list and the user's post list.
    var updates = {};
    updates['/posts2/' + newPostKey] = postData;
    updates['/user-posts2/' + uid + '/' + newPostKey] = postData;

    return firebase.database().ref().update(updates);
}
// [END write_fan_out]

/**
 * Star/unstar post.
 */
// [START post_stars_transaction]
function toggleStar(postRef, uid) {
    postRef.transaction(function(post) {
        if (post) {
            if (post.stars && post.stars[uid]) {
                post.starCount--;
                post.stars[uid] = null;
            } else {
                post.starCount++;
                if (!post.stars) {
                    post.stars = {};
                }
                post.stars[uid] = true;
            }
        }
        return post;
    });
}
// [END post_stars_transaction]

/**
 * Creates a post element.
 */



function createPostElement(postId, title, text, author, authorId, authorPic) {
    var uid = firebase.auth().currentUser.uid;
    //'<div class="avatar"></div>' +
    var html =
        '<div class="post post-' + postId + ' mdl-cell mdl-cell--12-col ' +
        'mdl-cell--6-col-tablet mdl-cell--4-col-desktop mdl-grid mdl-grid--no-spacing">' +
        '<div class="mdl-card mdl-shadow--2dp">' +
        '<div class="mdl-card__title mdl-color--light-blue-600 mdl-color-text--white">' +
        '<h4 class="mdl-card__title-text"></h4>' +
        '</div>' +
        '<div class="avatarmain"></div>' +
        '<div class="header">' +
        '<div>' +

        '<div class="username mdl-color-text--black"></div>' +
        '</div>' +
        '</div>' +
        '<span class="star">' +
        '<div class="not-starred material-icons">star_border</div>' +
        '<div class="starred material-icons">star</div>' +
        '<div class="star-count">0</div>' +
        '</span>' +
        '<div class="text"></div>' +
        '<div class="comments-container"></div>' +
        '<form class="add-comment" action="#">' +
        '<div class="mdl-textfield mdl-js-textfield">' +
        '<input class="mdl-textfield__input new-comment" type="text">' +
        '<label class="mdl-textfield__label">Comment...</label>' +
        '<a target=_blank href=https://play.google.com/store/apps/details?id=space.disableddating>Install app for make bet in DCT</a>' +
        '<button class="btn-adopt" type="button" data-id="0">Make Bet in ETH</button>' +
        '</div>' +
        '</form>' +

        '</div>' +
        '</div>';




    // Create the DOM element from the HTML.
    var div = document.createElement('div');
    div.innerHTML = html;
    var postElement = div.firstChild;
    if (componentHandler) {
        componentHandler.upgradeElements(postElement.getElementsByClassName('mdl-textfield')[0]);
    }

    var addCommentForm = postElement.getElementsByClassName('add-comment')[0];
    var commentInput = postElement.getElementsByClassName('new-comment')[0];
    var star = postElement.getElementsByClassName('starred')[0];
    var unStar = postElement.getElementsByClassName('not-starred')[0];

    // Set values.
    postElement.getElementsByClassName('text')[0].innerText = text;
    postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = title;
    postElement.getElementsByClassName('username')[0].innerText = author || 'Anonymous';
    //postElement.getElementsByClassName('avatar')[0].style.backgroundImage = 'url("' + (authorPic || './silhouette.jpg') + '")';
    postElement.getElementsByClassName('avatarmain')[0].style.backgroundImage = 'url("' + (authorPic || './silhouette.jpg') + '")';
    //$(postElement.getElementsByClassName('btn-adopt')[0]).attr('data-id', authorId);
    $(postElement.getElementsByClassName('btn-adopt')[0]).attr('authorId', authorId);
    $(postElement.getElementsByClassName('btn-adopt')[0]).attr('postId', postId);

    // Listen for comments.
    // [START child_event_listener_recycler]
    var commentsRef = firebase.database().ref('post-comments/' + postId);
    commentsRef.on('child_added', function(data) {
        addCommentElement(postElement, data.key, data.val().text, data.val().author);
    });

    commentsRef.on('child_changed', function(data) {
        setCommentValues(postElement, data.key, data.val().text, data.val().author);
    });

    commentsRef.on('child_removed', function(data) {
        deleteComment(postElement, data.key);
    });
    // [END child_event_listener_recycler]

    // Listen for likes counts.
    // [START post_value_event_listener]
    var starCountRef = firebase.database().ref('posts2/' + postId + '/starCount');
    starCountRef.on('value', function(snapshot) {
        updateStarCount(postElement, snapshot.val());
    });
    // [END post_value_event_listener]

    // Listen for the starred status.
    var starredStatusRef = firebase.database().ref('posts2/' + postId + '/stars/' + uid);
    starredStatusRef.on('value', function(snapshot) {
        updateStarredByCurrentUser(postElement, snapshot.val());
    });

    // Keep track of all Firebase reference on which we are listening.
    listeningFirebaseRefs.push(commentsRef);
    listeningFirebaseRefs.push(starCountRef);
    listeningFirebaseRefs.push(starredStatusRef);

    // Create new comment.
    addCommentForm.onsubmit = function(e) {
        e.preventDefault();
        createNewComment(postId, firebase.auth().currentUser.displayName, uid, commentInput.value);
        commentInput.value = '';
        commentInput.parentElement.MaterialTextfield.boundUpdateClassesHandler();
    };

    // Bind starring action.
    var onStarClicked = function() {
        var globalPostRef = firebase.database().ref('/posts2/' + postId);
        var userPostRef = firebase.database().ref('/user-posts2/' + authorId + '/' + postId);
        toggleStar(globalPostRef, uid);
        toggleStar(userPostRef, uid);
    };
    unStar.onclick = onStarClicked;
    star.onclick = onStarClicked;




    return postElement;
}

/**
 * Writes a new comment for the given post.
 */
function createNewComment(postId, username, uid, text) {
    firebase.database().ref('post-comments/' + postId).push({
        text: text,
        author: username,
        uid: uid
    });
}

/**
 * Updates the starred status of the post.
 */
function updateStarredByCurrentUser(postElement, starred) {
    if (starred) {
        postElement.getElementsByClassName('starred')[0].style.display = 'inline-block';
        postElement.getElementsByClassName('not-starred')[0].style.display = 'none';
    } else {
        postElement.getElementsByClassName('starred')[0].style.display = 'none';
        postElement.getElementsByClassName('not-starred')[0].style.display = 'inline-block';
    }
}

/**
 * Updates the number of stars displayed for a post.
 */
function updateStarCount(postElement, nbStart) {
    postElement.getElementsByClassName('star-count')[0].innerText = nbStart;
}

/**
 * Creates a comment element and adds it to the given postElement.
 */
function addCommentElement(postElement, id, text, author) {
    var comment = document.createElement('div');
    comment.classList.add('comment-' + id);
    comment.innerHTML = '<span class="username"></span><span class="comment"></span>';
    comment.getElementsByClassName('comment')[0].innerText = text;
    comment.getElementsByClassName('username')[0].innerText = author || 'Anonymous';

    var commentsContainer = postElement.getElementsByClassName('comments-container')[0];
    commentsContainer.appendChild(comment);
}

/**
 * Sets the comment's values in the given postElement.
 */
function setCommentValues(postElement, id, text, author) {
    var comment = postElement.getElementsByClassName('comment-' + id)[0];
    comment.getElementsByClassName('comment')[0].innerText = text;
    comment.getElementsByClassName('fp-username')[0].innerText = author;
}

/**
 * Deletes the comment of the given ID in the given postElement.
 */
function deleteComment(postElement, id) {
    var comment = postElement.getElementsByClassName('comment-' + id)[0];
    comment.parentElement.removeChild(comment);
}

/**
 * Starts listening for new posts and populates posts lists.
 */
function startDatabaseQueries() {
    // [START my_top_posts_query]
    var myUserId = firebase.auth().currentUser.uid;
    var topUserPostsRef = firebase.database().ref('user-posts2/' + myUserId).orderByChild('starCount');
    // [END my_top_posts_query]
    // [START recent_posts_query]
    var recentPostsRef = firebase.database().ref('posts2').limitToLast(100);
    // [END recent_posts_query]
    var userPostsRef = firebase.database().ref('user-posts2/' + myUserId);


    var fetchPosts = function(postsRef, sectionElement) {
        postsRef.on('child_added', function(data) {
            var author = data.val().author || 'Anonymous';
            var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
            containerElement.insertBefore(
                //createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().image),
                containerElement.firstChild);
        });
        postsRef.on('child_changed', function(data) {
            var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
            var postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
            postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
            postElement.getElementsByClassName('username')[0].innerText = data.val().author;
            postElement.getElementsByClassName('text')[0].innerText = data.val().body;
            postElement.getElementsByClassName('star-count')[0].innerText = data.val().starCount;
        });
        postsRef.on('child_removed', function(data) {
            var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
            var post = containerElement.getElementsByClassName('post-' + data.key)[0];
            post.parentElement.removeChild(post);
        });
    };

    // Fetching and displaying all posts of each sections.
    fetchPosts(topUserPostsRef, topUserPostsSection);
    fetchPosts(recentPostsRef, recentPostsSection);
    fetchPosts(userPostsRef, userPostsSection);

    // Keep track of all Firebase refs we are listening to.
    listeningFirebaseRefs.push(topUserPostsRef);
    listeningFirebaseRefs.push(recentPostsRef);
    listeningFirebaseRefs.push(userPostsRef);
}

/**
 * Writes the user's data to the database.
 */
// [START basic_write]
function writeUserData(userId, name, email, imageUrl) {
    firebase.database().ref('users/' + userId).set({
        username: name,
        email: email,
        profile_picture: imageUrl
    });
}
// [END basic_write]

/**
 * Cleanups the UI and removes all Firebase listeners.
 */
function cleanupUi() {
    // Remove all previously displayed posts.
    topUserPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
    recentPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
    userPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';

    // Stop all currently listening Firebase listeners.
    listeningFirebaseRefs.forEach(function(ref) {
        ref.off();
    });
    listeningFirebaseRefs = [];
}

/**
 * The ID of the currently signed-in User. We keep track of this to detect Auth state change events that are just
 * programmatic token refresh but not a User status change.
 */
var currentUID;

/**
 * Triggers every time there is a change in the Firebase auth state (i.e. user signed-in or user signed out).
 */
function onAuthStateChanged(user) {
    // We ignore token refresh events.
    if (user && currentUID === user.uid) {
        return;
    }

    cleanupUi();
    if (user) {
        currentUID = user.uid;
        splashPage.style.display = 'none';
        writeUserData(user.uid, user.displayName, user.email, user.photoURL);
        startDatabaseQueries();
    } else {
        // Set currentUID to null.
        currentUID = null;
        // Display the splash page where you can sign-in.
        splashPage.style.display = '';
    }
}

/**
 * Creates a new post for the current user.
 */
function newPostForCurrentUser(title, text) {
    // [START single_value_read]
    var userId = firebase.auth().currentUser.uid;
    return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
        var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        // [START_EXCLUDE]
        return writeNewPost(firebase.auth().currentUser.uid, username,
            firebase.auth().currentUser.photoURL,
            title, text);
        // [END_EXCLUDE]
    });
    // [END single_value_read]
}

/**
 * Displays the given section element and changes styling of the given button.
 */
function showSection(sectionElement, buttonElement) {
    recentPostsSection.style.display = 'none';
    userPostsSection.style.display = 'none';
    topUserPostsSection.style.display = 'none';
    addPost.style.display = 'none';
    recentMenuButton.classList.remove('is-active');
    myPostsMenuButton.classList.remove('is-active');
    myTopPostsMenuButton.classList.remove('is-active');

    if (sectionElement) {
        sectionElement.style.display = 'block';
    }
    if (buttonElement) {
        buttonElement.classList.add('is-active');
    }
}




// Bindings on load.
window.addEventListener('load', function() {

	startDatabaseQueries();

    $(document).on('click', '.btn-adopt', makeBet);


    // Bind Sign in button.
    signInButton.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider);
    });

    // Bind Sign out button.
    signOutButton.addEventListener('click', function() {
        firebase.auth().signOut();
    });

    // Listen for auth state changes
    firebase.auth().onAuthStateChanged(onAuthStateChanged);

    // Saves message on form submit.
    messageForm.onsubmit = function(e) {
        e.preventDefault();
        var text = messageInput.value;
        var title = titleInput.value;
        if (text && title) {
            newPostForCurrentUser(title, text).then(function() {
                myPostsMenuButton.click();
            });
            messageInput.value = '';
            titleInput.value = '';
        }
    };

    // Bind menu buttons.
    recentMenuButton.onclick = function() {
        showSection(recentPostsSection, recentMenuButton);
    };
    myPostsMenuButton.onclick = function() {
        showSection(userPostsSection, myPostsMenuButton);
    };
    myTopPostsMenuButton.onclick = function() {
        showSection(topUserPostsSection, myTopPostsMenuButton);
    };
    addButton.onclick = function() {
        showSection(addPost);
        messageInput.value = '';
        titleInput.value = '';
    };
    //App.initWeb3();
    recentMenuButton.onclick();




    /*
  App.init(); 
  
   // Is there an injected web3 instance?
if (typeof web3 !== 'undefined') {
  App.web3Provider = web3.currentProvider;
} else {
   noprovider=true;
}

web3 = new Web3(App.web3Provider);


 $.getJSON('Adoption.json', function(data) {
  // Get the necessary contract artifact file and instantiate it with truffle-contract
  
  if (!noprovider){  
  
  var AdoptionArtifact = data;
  App.contracts.Adoption = TruffleContract(AdoptionArtifact);

  // Set the provider for our contract
  App.contracts.Adoption.setProvider(App.web3Provider);
  }
 return App.markAdopted();
});
*/

    /*
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    // Use Mist/MetaMask's provider
    web3js = new Web3(web3.currentProvider);
  } else {
    console.log('No web3? You should consider trying MetaMask!');
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    web3js = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
  }

  // Now you can start your app & access web3 freely:
  //startApp();
  
  if (web3js) {
  if (web3js.eth.accounts.length) {
    // if not locked, get account
    const account = web3js.eth.accounts[0];
    // updates UI, state, pull data
  } else {
    // locked. update UI. Ask user to unlock.
  }
}

if (web3js) {
  switch (web3js.version.network) {
    case '1':
      console.log('This is mainnet');
      break;
    case '2':
      console.log('This is the deprecated Morden test network.');
      break;
    case '3':
      console.log('This is the ropsten test network.');
      break;
    case '4':
      console.log('This is the Rinkeby test network.');
      break;
    case '42':
      console.log('This is the Kovan test network.');
      break;
    default:
      console.log('This is an unknown network.');
  }

  const desiredNetwork = 1;
  if (web3js.version.network !== desiredNetwork) {
    // ask user to switch to desired network
    console.log('Please switch to main network.');
  }
}
*/
    /*
    var Tx = require('ethereumjs-tx');
    var Web3 = require('web3')
    var web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:8545/'))

    // set token source, destination and amount
    var myAddress = "0xaa597b7e8aaffe9f2a187bedb472ef3455957560"
    var toAddress = "0xa013927bffe9e879134061b9330a01884a65497c"
    var amount = web3.utils.toHex(1e16)

    // get transaction count, later will used as nonce
    web3.eth.getTransactionCount(myAddress).then(function(v){console.log(v); count = v})  

    // set your private key here, we'll sign the transaction below
    var privateKey = new Buffer('6d...', 'hex')  

    // Get abi array here https://etherscan.io/address/0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0#code
    var abiArray = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"stop","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"guy","type":"address"},{"name":"wad","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"owner_","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"src","type":"address"},{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"dst","type":"address"},{"name":"wad","type":"uint128"}],"name":"push","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name_","type":"bytes32"}],"name":"setName","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"wad","type":"uint128"}],"name":"mint","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"src","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"stopped","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"authority_","type":"address"}],"name":"setAuthority","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"src","type":"address"},{"name":"wad","type":"uint128"}],"name":"pull","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"wad","type":"uint128"}],"name":"burn","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"start","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"authority","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"src","type":"address"},{"name":"guy","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[{"name":"symbol_","type":"bytes32"}],"payable":false,"type":"constructor"},{"anonymous":true,"inputs":[{"indexed":true,"name":"sig","type":"bytes4"},{"indexed":true,"name":"guy","type":"address"},{"indexed":true,"name":"foo","type":"bytes32"},{"indexed":true,"name":"bar","type":"bytes32"},{"indexed":false,"name":"wad","type":"uint256"},{"indexed":false,"name":"fax","type":"bytes"}],"name":"LogNote","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"authority","type":"address"}],"name":"LogSetAuthority","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"}],"name":"LogSetOwner","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"}]
    // Here you may get the abicode from a string or a file, here is a string case
    // var abiArray = JSON.parse('[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"stop","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"guy","type":"address"},{"name":"wad","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"owner_","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"src","type":"address"},{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"dst","type":"address"},{"name":"wad","type":"uint128"}],"name":"push","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name_","type":"bytes32"}],"name":"setName","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"wad","type":"uint128"}],"name":"mint","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"src","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"stopped","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"authority_","type":"address"}],"name":"setAuthority","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"src","type":"address"},{"name":"wad","type":"uint128"}],"name":"pull","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"wad","type":"uint128"}],"name":"burn","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"bytes32"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"dst","type":"address"},{"name":"wad","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"start","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"authority","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"src","type":"address"},{"name":"guy","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[{"name":"symbol_","type":"bytes32"}],"payable":false,"type":"constructor"},{"anonymous":true,"inputs":[{"indexed":true,"name":"sig","type":"bytes4"},{"indexed":true,"name":"guy","type":"address"},{"indexed":true,"name":"foo","type":"bytes32"},{"indexed":true,"name":"bar","type":"bytes32"},{"indexed":false,"name":"wad","type":"uint256"},{"indexed":false,"name":"fax","type":"bytes"}],"name":"LogNote","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"authority","type":"address"}],"name":"LogSetAuthority","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"}],"name":"LogSetOwner","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"}]', 'utf-8')

    var contractAddress = '0x86Fa049857E0209aa7D9e616F7eb3b3B78ECfdb0'
    var contract = new web3.eth.Contract(abiArray, contractAddress, {from: myAddress})

    var rawTransaction = {"from":myAddress, "gasPrice":web3.utils.toHex(2 * 1e9),"gasLimit":web3.utils.toHex(210000),"to":contractAddress,"value":"0x0","data":contract.methods.transfer(toAddress, amount).encodeABI(),"nonce":web3.utils.toHex(count)} 
    var transaction = new Tx(rawTransaction)
    transaction.sign(privateKey)

    web3.eth.sendSignedTransaction('0x' + transaction.serialize().toString('hex'))

    // check the balance
    contract.methods.balanceOf(myAddress).call().then(function(balance){console.log(balance)})
    */

}, false);