var prices1 = []; // создать массив
let pricemap = new Map();
pricemap[1] = 0.0000000000000001;
pricemap[2] = 0.0000000000000001;
pricemap[3] = 0.0000000000000001;
pricemap[4] = 0.0000000000000001;
pricemap[5] = 0.0000000000000001;
pricemap[6] = 0.0000000000000001;
pricemap[7] = 0.0000000000000001;
pricemap[8] = 0.0000000000000001;
pricemap[9] = 0.0000000000000001;
pricemap[10] = 0.0000000000000001;
pricemap[11] = 0.0000000000000001;
pricemap[12] = 0.0000000000000001;
pricemap[13] = 0.0000000000000001;
pricemap[14] = 0.0000000000000001;
pricemap[15] = 0.0000000000000001;
pricemap[16] = 0.0000000000000001;
pricemap[17] = 0.0000000000000001;
pricemap[18] = 0.0000000000000001;
pricemap[19] = 0.0000000000000001;
pricemap[20] = 0.0000000000000001;
pricemap[21] = 0.0000000000000001;
pricemap[22] = 0.0000000000000001;
pricemap[23] = 0.0000000000000001;
pricemap[24] = 0.0000000000000001;
pricemap[25] = 0.0000000000000001;
pricemap[26] = 0.0000000000000001;
pricemap[27] = 0.0000000000000001;
pricemap[28] = 0.0000000000000001;
pricemap[29] = 0.0000000000000001;
pricemap[30] = 0.0000000000000001;
pricemap[31] = 0.0000000000000001;
pricemap[32] = 0.0000000000000001;
pricemap[33] = 0.0000000000000001;
pricemap[34] = 0.0000000000000001;
pricemap[35] = 0.0000000000000001;

// Shortcuts to DOM Elements.
var messageForm = document.getElementById('message-form');
var messageInput = document.getElementById('new-post-message');
var titleInput = document.getElementById('new-post-title');
var signInButton = document.getElementById('sign-in-button');
var signOutButton = document.getElementById('sign-out-button');
var splashPage = document.getElementById('page-splash');
var addPost = document.getElementById('add-post');
var addButton = document.getElementById('add');
var recentPostsSection = document.getElementById('recent-posts-list');
var userPostsSection = document.getElementById('user-posts-list');
var topUserPostsSection = document.getElementById('top-user-posts-list');
var recentMenuButton = document.getElementById('menu-recent');
var myPostsMenuButton = document.getElementById('menu-my-posts');
var myTopPostsMenuButton = document.getElementById('menu-my-top-posts');
var listeningFirebaseRefs = [];
var noprovider = false;
var currentUID;


//вкладки
var tab; // заголовок вкладки
var tabContent; // блок содержащий контент вкладки



$.fn.popup = function(petId, petPrice) { //функция для открытия всплывающего окна:
    this.css('position', 'absolute').fadeIn(); //задаем абсолютное позиционирование и эффект открытия
    //махинации с положением сверху:учитывается высота самого блока, экрана и позиции на странице:
    this.css('top', ($(window).height() - this.height()) / 2 + $(window).scrollTop() + 'px');
    //слева задается отступ, учитывается ширина самого блока и половина ширины экрана
    this.css('left', ($(window).width() - this.width()) / 2 + 'px');
    //открываем тень с эффектом:
    $('.backpopup').fadeIn();
    //установим в скрытых полях
    document.getElementById("petId").value = petId;
    document.getElementById("price").value = petPrice;

}



$(document).ready(function() { //при загрузке страницы:



    $('.backpopup,.close').click(function() { //событие клик на тень и крестик - закрываем окно и тень:
        $('.popup-window').fadeOut();
        $('.backpopup').fadeOut();
    });
});


App = {
    web3Provider: null,
    contracts: {},
	
    init: function() {
        // Load pets.
        $.getJSON('pets.json', function(data) {
            var petsRow = $('#petsRow');
            var petTemplate = $('#petTemplate');

            for (i = 0; i < data.length; i++) {

                petTemplate.find('.panel-title').text(data[i].name);
                petTemplate.find('img').attr('src', data[i].picture);
                petTemplate.find('.pet-breed').text(data[i].breed);
                petTemplate.find('.pet-price').text(data[i].price);
                petTemplate.find('.pet-earn').text(data[i].earn);
                petTemplate.find('.btn-adopt').attr('data-id', data[i].id);
                petsRow.append(petTemplate.html());
            }
        });

        return App.initWeb3();
    },
    initWeb3: function() {
        // Is there an injected web3 instance?
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider;
        } else {
            noprovider = true;
        }
        web3 = new Web3(App.web3Provider);

        return App.initContract();
    },
    initContract: function() {
       
	   $.getJSON('SimpleAuction.json', function(data) {
            if (!noprovider) {
                var SimpleArtifact = data;
                App.contracts.Simple = TruffleContract(SimpleArtifact);
                App.contracts.Simple.setProvider(App.web3Provider);
            }
        });

	   $.getJSON('Adoption.json', function(data) {
            if (!noprovider) {
                var AdoptionArtifact = data;
                App.contracts.Adoption = TruffleContract(AdoptionArtifact);
                App.contracts.Adoption.setProvider(App.web3Provider);
                App.markPrices();            
            }
            return App.markAdopted();
        });

        return App.bindEvents();
    },

    bindEvents: function() {
        $(document).on('click', '.btn-adopt', App.handleAdopt);
		$(document).on('click', '.btn-bet', App.handleBet);
        $(document).on('click', '.btn-info', App.handleInfo);
		
    },
    markAdopted: function(adopters, account) {
        var adoptionInstance;

        App.contracts.Adoption.deployed().then(function(instance) {
            adoptionInstance = instance;

            return adoptionInstance.getAdopters.call(); //тут получаем айдишники продаваемых животных
        }).then(function(adopters) {
            for (i = 0; i < adopters.length; i++) {
                if (adopters[i] !== '0x0000000000000000000000000000000000000000') { //!! всегда выполняется
                    //if (adopters[i] !== '0x') {	
                    //if (adopters[i] !== 0) {
                    //alert(adopters[i]);
                    //$('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true);
                    //$('.panel-pet').eq(i).find('button').text('Success').attr('enabled', true);
                    // $('.panel-pet').eq(i).find('button').attr('data-id', adopters[i]);											//установим айдишники продаваемых петов, придумать куда засунуть цену
                    //$('.panel-pet').eq(i).find('button').attr('text', prices1[i]);
                }


            }
        }).catch(function(err) {
            console.log(err.message);
        });
    },
    markPrices: function(prices, account) {
        var adoptionInstance;

        App.contracts.Adoption.deployed().then(function(instance) {
            adoptionInstance = instance;

            return adoptionInstance.getPrices.call();
        }).then(function(prices) {
            //for (i = 0; i < prices.length; i++) {													//это кажется не работает, как отладить?
            for (i = 0; i < 35; i++) {

                if (prices[i] != 0) {
                    //alert (prices[i]);
                    $('.panel-pet').eq(i).find('.pet-price').text(prices[i] / 1000000000000000000);
                    prices1.push(prices[i]); //запишем цену в массив

                } else {
                    //alert (prices[i]);
                    $('.panel-pet').eq(i).find('.pet-price').text("1");
                    prices1.push(0.01);
                }
            }
        }).catch(function(err) {
            console.log(err.message);
        });
    },
	handleBet: function(event) {
        event.preventDefault();
        //var petPrice = pricemap[petId]; //где это записать
		//var petId = parseInt($(event.target).data('id'));
		var betPrice = 1;
		//alert ("eventtarget:",event.target);
		//var postId = $(event.target).getAttribute("postid");
		//var postId = '-Lbuz_KfWKHl8uD8Yg_f';
		var postId=2;
        var adoptionInstance;
		
        if (!noprovider) {

            web3.eth.getAccounts(function(error, accounts) {
                if (error) {
                    console.log(error);
                }

                var account = accounts[0];

                App.contracts.Simple.deployed().then(function(instance) {
                    adoptionInstance = instance;
					/*		
                    return adoptionInstance.adopt(petId, {
                        from: account,
                        value: web3.toWei(petPrice, "ether"),
                        gas: 4600000,
                        to: "0x7c5522ac06d0721b3b31b37b63078bcc719c74aa"
                    
					});*/
					
					console.log("postId:", postId);
					console.log("betPrice:", betPrice);
					console.log("instance:", instance);
					console.log("account:", account);
					return instance.bid(postId, {from: account,value: web3.toWei(betPrice, "ether"),gas: 300000,to: "0x933fa37ae7ec7f2b6082a0e51d8f9c9a8f7662e1"});
					//return instance.withdraw();
					
                }).then(function(result) {
                    return App.markAdopted();
                }).catch(function(err) {
                    console.log(err.message);
                });
            });
        } else {

            $('.popup-window').popup(petId, petPrice); //открыть всплывающее окно при загрузке сайта
        }
    },
    handleAdopt: function(event) {
        event.preventDefault();

        var petId = parseInt($(event.target).data('id'));
		
		
		
		
        var petPrice = pricemap[petId]; //где это записать
        //var petPrice = 0.1;

        var adoptionInstance;

        if (!noprovider) {
            web3.eth.getAccounts(function(error, accounts) {
                if (error) {
                    console.log(error);
                }

                var account = accounts[0];

                App.contracts.Adoption.deployed().then(function(instance) {
                    adoptionInstance = instance;
                    return adoptionInstance.adopt(petId, {
                        from: account,
                        value: web3.toWei(petPrice, "ether"),
                        gas: 4600000,
                        to: "0x7c5522ac06d0721b3b31b37b63078bcc719c74aa"
                    });

                }).then(function(result) {
                    return App.markAdopted();
                }).catch(function(err) {
                    console.log(err.message);
                });
            });
        } else {
            $('.popup-window').popup(petId, petPrice); //открыть всплывающее окно при загрузке сайта
        }

    }
};








//тут

function writeNewPost(uid, username, picture, title, body) {
    // A post entry.
    var postData = {
        author: username,
        uid: uid,
        body: body,
        title: title,
        starCount: 0,
        authorPic: picture
    };

    // Get a key for a new Post.
    var newPostKey = firebase.database().ref().child('posts2').push().key;

    // Write the new post's data simultaneously in the posts list and the user's post list.
    var updates = {};
    updates['/posts2/' + newPostKey] = postData;
    updates['/user-posts2/' + uid + '/' + newPostKey] = postData;

    return firebase.database().ref().update(updates);
}
// [START post_stars_transaction]
function toggleStar(postRef, uid) {
    postRef.transaction(function(post) {
        if (post) {
            if (post.stars && post.stars[uid]) {
                post.starCount--;
                post.stars[uid] = null;
            } else {
                post.starCount++;
                if (!post.stars) {
                    post.stars = {};
                }
                post.stars[uid] = true;
            }
        }
        return post;
    });
}
// [END post_stars_transaction]

/**
 * Creates a post element.
 */

 var topbet;	//топ ставка
 
function createPostElement(postId, title, text, author, authorId, authorPic) {
    var uid = firebase.auth().currentUser.uid;
    //'<div class="avatar"></div>' +
    var html =
        '<div class="post post-' + postId + ' mdl-cell mdl-cell--12-col ' +
        'mdl-cell--6-col-tablet mdl-cell--4-col-desktop mdl-grid mdl-grid--no-spacing">' +
        '<div class="mdl-card mdl-shadow--2dp">' +
        '<div class="mdl-card__title mdl-color--light-blue-600 mdl-color-text--white">' +
        '<h4 class="mdl-card__title-text" data-id=0></h4>' +
		'<h4 class="mdl-topbet" data-id=0></h4>' +
        '</div>' +
        '<div class="avatarmain"></div>' +
        '<div class="header">' +
        '<div>' +

        '<div class="username mdl-color-text--black"></div>' +
        '</div>' +
        '</div>' +
        '<span class="star">' +
        '<div class="not-starred material-icons">star_border</div>' +
        '<div class="starred material-icons">star</div>' +
        '<div class="star-count">0</div>' +
        '</span>' +
        '<div class="text"></div>' +
        '<div class="comments-container"></div>' +
        '<form class="add-comment" action="#">' +
        '<div class="mdl-textfield mdl-js-textfield">' +
        '<input class="mdl-textfield__input new-comment" type="text">' +
        '<label class="mdl-textfield__label">Comment...</label>' +
        '<a target=_blank href=https://play.google.com/store/apps/details?id=space.disableddating>Install app for make bet in DCT</a>' +
        '<button class="btn-bet" type="button" data-id="0">Make Bet in ETH</button>' +
		'<button class="btn-adopt" type="button" data-id="0">Adopt</button><br>' +
		'<button class="btn-plus100bet" type="button" data-id="0">+100</button>' +
		'<button class="btn-plus500bet" type="button" data-id="0">+500</button>' +
		'<button class="btn-plus1000bet" type="button" data-id="0">+1000</button><br>' +
		'<input class="mybet" maxlength="25" size="40">' +
		'<button class="mybet" type="button" data-id="0">My Bet</button>' +
        '</div>' +
        '</form>' +

        '</div>' +
        '</div>';




    // Create the DOM element from the HTML.
    var div = document.createElement('div');
    div.innerHTML = html;
    var postElement = div.firstChild;
    if (componentHandler) {
        componentHandler.upgradeElements(postElement.getElementsByClassName('mdl-textfield')[0]);
    }

    var addCommentForm = postElement.getElementsByClassName('add-comment')[0];
    var commentInput = postElement.getElementsByClassName('new-comment')[0];
    var star = postElement.getElementsByClassName('starred')[0];
    var unStar = postElement.getElementsByClassName('not-starred')[0];

    // Set values.
    postElement.getElementsByClassName('text')[0].innerText = text;
    postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = title;
    postElement.getElementsByClassName('username')[0].innerText = author || 'Anonymous';
    //postElement.getElementsByClassName('avatar')[0].style.backgroundImage = 'url("' + (authorPic || './silhouette.jpg') + '")';
    postElement.getElementsByClassName('avatarmain')[0].style.backgroundImage = 'url("' + (authorPic || './silhouette.jpg') + '")';
    //$(postElement.getElementsByClassName('btn-bet')[0]).attr('data-id', authorId);
    $(postElement.getElementsByClassName('btn-bet')[0]).attr('authorId', authorId);
    $(postElement.getElementsByClassName('btn-bet')[0]).attr('postId', postId);
	 $(postElement.getElementsByClassName('btn-adopt')[0]).attr('data-id', postId);
	 $(postElement.getElementsByClassName('btn-plus100bet')[0]).attr('data-id', postId);
	 $(postElement.getElementsByClassName('btn-plus500bet')[0]).attr('data-id', postId);
	 $(postElement.getElementsByClassName('btn-plus1000bet')[0]).attr('data-id', postId);
	 $(postElement.getElementsByClassName('mdl-topbet')[0]).attr('data-id', postId);
	  $(postElement.getElementsByClassName('mybet')[0]).attr('data-id', postId);
	 
	 
	 //$(postElement.getElementsByClassName('mdl-card__title-text')[0]).attr('data-id', postId);
	
	 

    // Listen for comments.
    // [START child_event_listener_recycler]
    var commentsRef = firebase.database().ref('post-comments/' + postId);
    
	/*
	
	var betsRef = firebase.database().ref('post-bets/' + postId);
	var postsRef = firebase.database().ref('posts2');
	
									
	
	postsRef.ref(uid+'/top_bet').once('value').then(function(snapshot) {
		var topbet = snapshot.val().summ;
		postElement.getElementsByClassName('mdl-topbet')[0].innerText=topbet;
	});
	*/
	
	
	commentsRef.on('child_added', function(data) {
        addCommentElement(postElement, data.key, data.val().text, data.val().author);
    });

    commentsRef.on('child_changed', function(data) {
        setCommentValues(postElement, data.key, data.val().text, data.val().author);
    });

    commentsRef.on('child_removed', function(data) {
        deleteComment(postElement, data.key);
    });
    // [END child_event_listener_recycler]

    // Listen for likes counts.
    // [START post_value_event_listener]
    var starCountRef = firebase.database().ref('posts2/' + postId + '/starCount');
    starCountRef.on('value', function(snapshot) {
        updateStarCount(postElement, snapshot.val());
    });
    // [END post_value_event_listener]

    // Listen for the starred status.
    var starredStatusRef = firebase.database().ref('posts2/' + postId + '/stars/' + uid);
    starredStatusRef.on('value', function(snapshot) {
        updateStarredByCurrentUser(postElement, snapshot.val());
    });

    // Keep track of all Firebase reference on which we are listening.
    listeningFirebaseRefs.push(commentsRef);
    listeningFirebaseRefs.push(starCountRef);
    listeningFirebaseRefs.push(starredStatusRef);

    // Create new comment.
    addCommentForm.onsubmit = function(e) {
        e.preventDefault();
        createNewComment(postId, firebase.auth().currentUser.displayName, uid, commentInput.value);
        commentInput.value = '';
        commentInput.parentElement.MaterialTextfield.boundUpdateClassesHandler();
    };

    // Bind starring action.
    var onStarClicked = function() {
        var globalPostRef = firebase.database().ref('/posts2/' + postId);
        var userPostRef = firebase.database().ref('/user-posts2/' + authorId + '/' + postId);
        toggleStar(globalPostRef, uid);
        toggleStar(userPostRef, uid);
    };
    unStar.onclick = onStarClicked;
    star.onclick = onStarClicked;

    return postElement;
}

/**
 * Writes a new comment for the given post.
 */
function createNewComment(postId, username, uid, text) {
    firebase.database().ref('post-comments/' + postId).push({
        text: text,
        author: username,
        uid: uid
    });
}

/**
 * Updates the starred status of the post.
 */
function updateStarredByCurrentUser(postElement, starred) {
    if (starred) {
        postElement.getElementsByClassName('starred')[0].style.display = 'inline-block';
        postElement.getElementsByClassName('not-starred')[0].style.display = 'none';
    } else {
        postElement.getElementsByClassName('starred')[0].style.display = 'none';
        postElement.getElementsByClassName('not-starred')[0].style.display = 'inline-block';
    }
}

/**
 * Updates the number of stars displayed for a post.
 */
function updateStarCount(postElement, nbStart) {
    postElement.getElementsByClassName('star-count')[0].innerText = nbStart;
}

/**
 * Creates a comment element and adds it to the given postElement.
 */
function addCommentElement(postElement, id, text, author) {
    var comment = document.createElement('div');
    comment.classList.add('comment-' + id);
    comment.innerHTML = '<span class="username"></span><span class="comment"></span>';
    comment.getElementsByClassName('comment')[0].innerText = text;
    comment.getElementsByClassName('username')[0].innerText = author || 'Anonymous';

    var commentsContainer = postElement.getElementsByClassName('comments-container')[0];
    commentsContainer.appendChild(comment);
}

/**
 * Sets the comment's values in the given postElement.
 */
function setCommentValues(postElement, id, text, author) {
    var comment = postElement.getElementsByClassName('comment-' + id)[0];
    comment.getElementsByClassName('comment')[0].innerText = text;
    comment.getElementsByClassName('fp-username')[0].innerText = author;
}

/**
 * Deletes the comment of the given ID in the given postElement.
 */
handledeleteComment: function deleteComment(postElement, id) {
    var comment = postElement.getElementsByClassName('comment-' + id)[0];
    comment.parentElement.removeChild(comment);
}

/**
 * Starts listening for new posts and populates posts lists.
 */
function startDatabaseQueries() {
    // [START my_top_posts_query]
    var myUserId = firebase.auth().currentUser.uid;
    var topUserPostsRef = firebase.database().ref('user-posts2/' + myUserId).orderByChild('starCount');
    // [END my_top_posts_query]
    // [START recent_posts_query]
    var recentPostsRef = firebase.database().ref('posts2').limitToLast(100);
    // [END recent_posts_query]
    var userPostsRef = firebase.database().ref('user-posts2/' + myUserId);


    var fetchPosts = function(postsRef, sectionElement) {
        postsRef.on('child_added', function(data) {
            var author = data.val().author || 'Anonymous';
            var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
            containerElement.insertBefore(
                //createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().image),
                containerElement.firstChild);
        });
        postsRef.on('child_changed', function(data) {
            var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
            var postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
            postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
            postElement.getElementsByClassName('username')[0].innerText = data.val().author;
            postElement.getElementsByClassName('text')[0].innerText = data.val().body;
            postElement.getElementsByClassName('star-count')[0].innerText = data.val().starCount;
        });
        postsRef.on('child_removed', function(data) {
            var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
            var post = containerElement.getElementsByClassName('post-' + data.key)[0];
            post.parentElement.removeChild(post);
        });
    };

    // Fetching and displaying all posts of each sections.
    fetchPosts(topUserPostsRef, topUserPostsSection);
    fetchPosts(recentPostsRef, recentPostsSection);
    fetchPosts(userPostsRef, userPostsSection);

    // Keep track of all Firebase refs we are listening to.
    listeningFirebaseRefs.push(topUserPostsRef);
    listeningFirebaseRefs.push(recentPostsRef);
    listeningFirebaseRefs.push(userPostsRef);
}

/**
 * Writes the user's data to the database.
 */
// [START basic_write]
function writeUserData(userId, name, email, imageUrl) {
    firebase.database().ref('users/' + userId).set({
        username: name,
        email: email,
        profile_picture: imageUrl
    });
}
// [END basic_write]

/**
 * Cleanups the UI and removes all Firebase listeners.
 */
function cleanupUi() {
    // Remove all previously displayed posts.
    topUserPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
    recentPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
    userPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';

    // Stop all currently listening Firebase listeners.
    listeningFirebaseRefs.forEach(function(ref) {
        ref.off();
    });
    listeningFirebaseRefs = [];
}

/**
 * The ID of the currently signed-in User. We keep track of this to detect Auth state change events that are just
 * programmatic token refresh but not a User status change.
 */
//var currentUID;

/**
 * Triggers every time there is a change in the Firebase auth state (i.e. user signed-in or user signed out).
 */
function onAuthStateChanged(user) {
    // We ignore token refresh events.
    if (user && currentUID === user.uid) {
        return;
    }

    cleanupUi();
    if (user) {
        currentUID = user.uid;
		//alert (currentUID);
        splashPage.style.display = 'none';
        writeUserData(user.uid, user.displayName, user.email, user.photoURL);
        startDatabaseQueries();
		
		//вкладки
		tabContent=document.getElementsByClassName('tabContent');
		tab=document.getElementsByClassName('tab');
		hideTabsContent(1);	
		document.getElementById('tabs').onclick= function (event) {
		var target=event.target;
		if (target.className=='tab') {
		for (var i=0; i<tab.length; i++) {
           if (target == tab[i]) {
               showTabsContent(i);
               break;
		}}}}
    } else {
        // Set currentUID to null.
        currentUID = null;
        // Display the splash page where you can sign-in.
        splashPage.style.display = '';
    }
}

/**
 * Creates a new post for the current user.
 */
function newPostForCurrentUser(title, text) {
    // [START single_value_read]
    var userId = firebase.auth().currentUser.uid;
    return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
        var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        // [START_EXCLUDE]
        return writeNewPost(firebase.auth().currentUser.uid, username,
            firebase.auth().currentUser.photoURL,
            title, text);
        // [END_EXCLUDE]
    });
    // [END single_value_read]
}

/**
 * Displays the given section element and changes styling of the given button.
 */
function showSection(sectionElement, buttonElement) {
    recentPostsSection.style.display = 'none';
    userPostsSection.style.display = 'none';
    topUserPostsSection.style.display = 'none';
    addPost.style.display = 'none';
    recentMenuButton.classList.remove('is-active');
    myPostsMenuButton.classList.remove('is-active');
    myTopPostsMenuButton.classList.remove('is-active');

    if (sectionElement) {
        sectionElement.style.display = 'block';
    }
    if (buttonElement) {
        buttonElement.classList.add('is-active');
    }
}


function hideTabsContent(a) {
    for (var i=a; i<tabContent.length; i++) {
        tabContent[i].classList.remove('show');
        tabContent[i].classList.add("hide");
        tab[i].classList.remove('whiteborder');
    }
}

function showTabsContent(b){
    if (tabContent[b].classList.contains('hide')) {
        hideTabsContent(0);
        tab[b].classList.add('whiteborder');
        tabContent[b].classList.remove('hide');
        tabContent[b].classList.add('show');
    }
}



function makeBet(betamount)
{
	var uid = firebase.auth().currentUser.uid;
	var postsRef = firebase.database();
	//alert (postsRef);
	var curid=$(event.target).data('id');
	//alert (curid);
	
	/*
	postsRef.ref('posts2/' + curid+'/top_bet').set({
    summ: 150
	});
	*/
	
	postsRef.ref('posts2/'+curid+'/top_bet/summ').once('value').then(function(snapshot) {
		var topbet = snapshot.val();
		//alert ("bet"+topbet);
		var newbet;
		if (topbet!=null)
              newbet = topbet+betamount;
        else
              newbet = 0+betamount;
		  
		alert ("newbet"+newbet);
		
		//установим новую ставку
		postsRef.ref('posts2/' + curid+'/top_bet').set({
			uid: uid,
			summ: newbet
		});
		
		//document.getElementById("data-id="+curid).innerText=newbet;
		
		//$('[class = "mdl-topbet"]');
		
		$('.mdl-topbet[data-id="-Lc4mjn-EeWy4_y7OCeq"]').innerText=newbet;
		
		//$(.mdl-topbet).innerText=topbet;
		
		//var data_id = document.getAttribute('data-id');
		//var dropdown_content_ths = document.querySelector('.dropdown__content[data-content='+data_id+']');
		//dropdown_content_th.innerText=topbet;
		//postElement.getElementsByClassName('mdl-topbet')[0].innerText=topbet;
	});
}

function makeBetplus100(){
	makeBet(100);
}
function makeBetplus500(){
	makeBet(500);
}
function makeBetplus1000(){
	makeBet(1000);
}
function makeBetmy(){
	
	//document.getElementById("data-id="+curid)
	makeBet(mybet);
}




$(function() {
    $(window).load(function() {
	
	
	
	App.init();
		
	 $(document).on('click', '.btn-plus100bet',makeBetplus100);
	 $(document).on('click', '.btn-plus500bet',makeBetplus500);
	 $(document).on('click', '.btn-plus1000bet',makeBetplus1000);
	 $(document).on('click', '.mybet',makeBetmy);
		
	//App.handlestartDatabaseQueries();
		
	//$(document).on('click', '.btn-adopt', makeBet);

		 
	//startDatabaseQueries();
	
	//$(document).on('click', '.btn-bet', App.handleBet);


// Shortcuts to DOM Elements.
var messageForm = document.getElementById('message-form');
var messageInput = document.getElementById('new-post-message');
var titleInput = document.getElementById('new-post-title');
var signInButton = document.getElementById('sign-in-button');
var signOutButton = document.getElementById('sign-out-button');
var splashPage = document.getElementById('page-splash');
var addPost = document.getElementById('add-post');
var addButton = document.getElementById('add');
var recentPostsSection = document.getElementById('recent-posts-list');
var userPostsSection = document.getElementById('user-posts-list');
var topUserPostsSection = document.getElementById('top-user-posts-list');
var recentMenuButton = document.getElementById('menu-recent');
var myPostsMenuButton = document.getElementById('menu-my-posts');
var myTopPostsMenuButton = document.getElementById('menu-my-top-posts');
var listeningFirebaseRefs = [];
var noprovider = false;
var currentUID;
	
		 

    // Bind Sign in button.
    signInButton.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider);
    });
	

    // Bind Sign out button.
    signOutButton.addEventListener('click', function() {
        firebase.auth().signOut();
    });

    // Listen for auth state changes
    firebase.auth().onAuthStateChanged(onAuthStateChanged);

    // Saves message on form submit.
    messageForm.onsubmit = function(e) {
        e.preventDefault();
        var text = messageInput.value;
        var title = titleInput.value;
        if (text && title) {
            newPostForCurrentUser(title, text).then(function() {
                myPostsMenuButton.click();
            });
            messageInput.value = '';
            titleInput.value = '';
        }
    };

    // Bind menu buttons.
    recentMenuButton.onclick = function() {
        showSection(recentPostsSection, recentMenuButton);
    };
    myPostsMenuButton.onclick = function() {
        showSection(userPostsSection, myPostsMenuButton);
    };
    myTopPostsMenuButton.onclick = function() {
        showSection(topUserPostsSection, myTopPostsMenuButton);
    };
    addButton.onclick = function() {
        showSection(addPost);
        messageInput.value = '';
        titleInput.value = '';
    };

    recentMenuButton.onclick();
		
		
		
    });
});