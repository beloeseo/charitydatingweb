pragma solidity ^0.4.17;


contract owned {

    address public owner;

    function owned() public payable {
        owner = msg.sender;
    }
    
    modifier onlyOwner {
        require(owner == msg.sender);
        _;
    }

    function changeOwner(address _owner) onlyOwner public {
        owner = _owner;
    }
}


contract ERC721 is owned{
   string constant public tokenName = "Crypto Animal Token";
   string constant public tokenSymbol = "CAT";
   uint256 constant private totalTokens = 1000000;
   mapping(address => uint) private balances;
   mapping(uint256 => address) public tokenOwners;                          //id токена => овнер токена
   mapping(uint256 => bool) public tokenExists;								//petid=>bool
   mapping(address => mapping (address => uint256)) private allowed;
   mapping(address => mapping(uint256 => uint256)) public ownerTokens;      //адрес => по индексу токены
   
   mapping(uint256 => string) public tokenOwnersIPFS;                       //адрес в ipfs
   mapping(uint256 => string) public tokenOwnersBMP;                        //bmp код
   mapping(uint256 => string) public descriptions;                         //description
   mapping(uint256 => string) public names;                                //name
   mapping(uint256 => uint256) public contractprices;                       //petid=>price цены контракта, устанавливаемые автоматически
   
   mapping(uint256 => uint256) public breed;                                //айди токена => порода животного
   mapping(uint256 => uint256) public breedcounter;                         //айди породы => количество проданных данного вида
   mapping(uint256 => uint256) public breedprices;                          //айди породы => цена на эту породу формируемая автоматически
 
   mapping(uint256 => mapping(uint256 => address)) public breedTokens;      //одна порода => айдишники всех её владельцев по индексу
          //вид              //индекс   //адрес владельца
   mapping(address => uint256) public rewards;                              //адрес владельца => размер награды за владение животным
   mapping(uint256 => uint256) public breedRate;                            //порода => ставка награды за породу
   uint256 private upReward;                                                //приращение награды
   bool approvalrewards=true;
   uint256 private lastRewardIndex=100;                                         //максимальный индекс за который даём награду    
   mapping(uint256 => bool) public breedUpPrice;                            //порода => используется ли рост цен на породу при новой покупке питомца этой породы
    mapping(uint256 => uint256) public fathers; 
    mapping(uint256 => uint256) public mothers; 
   mapping(uint256 => string) tokenLinks;                                   //доп инфо
   
   //для магазана
   address[100] public adopters;
   mapping(uint256 => uint256) public shopPetId;                            //petId=>id в магазне айдишники продаваймых животных
   uint[100] public    shopprices;		                                    //цены зверей в магазине
   uint[100] public    earns;	
   string[100] public imagesIpfs;                                           //ipfs адреса
   string[100] public imagesBmp;                                            //bmp картинки

mapping (address => bool) public frozenAccount;                             //замороженные аккаунты
   
   function ERC721() public{
      
        balances[this] = totalTokens;
       
		createAnimal(1,55,"ipfs1","bmp1","desc","name",0,0);
		createAnimal(2,55,"ipfs1","bmp1","desc","name",0,0);
		createAnimal(3,55,"ipfs1","bmp1","desc","name",0,0);
		createAnimal(4,55,"ipfs1","bmp1","desc","name",0,0);
		createAnimal(5,55,"ipfs1","bmp1","desc","name",0,0);
		createAnimal(6,55,"ipfs1","bmp1","desc","name",0,0);
		createAnimal(7,55,"ipfs1","bmp1","desc","name",0,0);
		createAnimal(8,55,"ipfs1","bmp1","desc","name",0,0);
		breedUpPrice[55]=true;
		setUpReward(3000000000000000);					//0.003 цена растёт быстрее в 10 раз чем награды
		contractprices[0]=100000000000000000;
		contractprices[1]=100000000000000000;
		contractprices[2]=100000000000000000;
		contractprices[3]=100000000000000000;
		contractprices[4]=100000000000000000;
		contractprices[5]=100000000000000000;
		contractprices[6]=100000000000000000;
		contractprices[7]=100000000000000000;
		contractprices[8]=100000000000000000;

		
		breedRate[55]=300000000000000;			//0.0003
		shopprices[0]=100000000000000000;
		shopprices[1]=100000000000000000;
		shopprices[2]=100000000000000000;
		shopprices[3]=100000000000000000;
		shopprices[4]=100000000000000000;
		shopprices[5]=100000000000000000;
		shopprices[6]=100000000000000000;
		shopprices[7]=100000000000000000;
		shopprices[8]=100000000000000000;
	
		shopPetId[1]=0;     //соответствие айдишников
		shopPetId[2]=1;
		shopPetId[3]=2;
		shopPetId[4]=3;
		shopPetId[5]=4;
		shopPetId[6]=5;
		shopPetId[7]=6;
		shopPetId[8]=7;
		
		earns[1]=300000000000000;		//заработки на породе
		earns[2]=300000000000000;
		earns[3]=300000000000000;
		earns[4]=300000000000000;
		earns[5]=300000000000000;
		earns[6]=300000000000000;
		earns[7]=300000000000000;
		earns[8]=300000000000000;

   }
   
   
   //удаления из списка владельцев токенов по индексу
   function removeFromTokenList(address owner, uint256 _tokenId) private {
        //найдём индекс токена для удаления
        uint i=0;
        while (ownerTokens[owner][i]!=_tokenId){
            i++;
        }
        ownerTokens[owner][i] = 0;
   }

   //удаления из списка породы и адреса владельцев токенов по индексу
   function removeFromBreedList(address owner, uint256 newbreed) private {
        uint i=0;
         while (breedTokens[newbreed][i]!=owner){
            i++;
        }
        breedTokens[newbreed][i] = 0;
   }
   
 
    function addToTokenList(address owner, uint256 _tokenId) private {
        //узнаем самый большой индекс
        uint256 i=0;
        while (ownerTokens[owner][i]!=0) { 
          i++;
        }
       ownerTokens[owner][i] = _tokenId;
    }
   
   //добавим в список всех владельцев одной породы (например утки) по индексам
    function addToBreedList(address owner, uint256 newbreed) private {
        //узнаем самый большой индекс
        uint256 i=0;
        while (breedTokens[newbreed][i]!=0) { 
          i++;
        }
        breedTokens[newbreed][i] = owner;
    }
   
   

   
   //вернуть просто массив токенов конкретного юзера
   /*
    function getUserTokenList(address owner, uint256 maxindex) public returns (uint256[]){
        uint256[] countanimals;
        uint256 i=0;
        uint256 j=0;
        //переделать на максимальный индекс
        //while (ownerTokens[owner][i]!=0) {  //если равно 0 или предыдущую очищенную ¤чейку, тогда мы нашли свободный индекс!
        while (i<maxindex){
            if (ownerTokens[owner][i]!=0) {
                countanimals[j]=ownerTokens[owner][i];
                j++;
            }
            i++;
        }
       return countanimals;
   }*/
   
   //для каждого животного конкретного владельца потом прочитать, получим айди токенов животных и с ними сделать ещё запрос на получение животных или как проще
   function getUserToken(address owner, uint256 tokenindex) public view returns (uint256){
       return ownerTokens[owner][tokenindex];
   }
   
   function name() public pure returns (string){
       return tokenName;
   }
   function symbol() public pure returns (string) {
       return tokenSymbol;
   }
   //вот это нафига
   function totalSupply() public pure returns (uint256){
       return totalTokens;
   }
   function balanceOf(address _owner) public constant returns (uint){
       return balances[_owner];
   }
   function ownerOf(uint256 _tokenId) public constant returns (address){
       require(tokenExists[_tokenId]);
       return tokenOwners[_tokenId];
   }
   
   //разрешить перевести на определенный адрес (не используется, задействовать в переводах прибыли)
   function approve(address _to, uint256 _tokenId) private{
       require(msg.sender == ownerOf(_tokenId));
       require(msg.sender != _to);
       allowed[msg.sender][_to] = _tokenId;
       Approval(msg.sender, _to, _tokenId);
   }
   
   //расчёт награды, сделать по запросу
    function countReward(uint256 newbreed) private returns (bool) {
        //максимум до сотого животного одной породы начисляем
        for (uint i=0; i<lastRewardIndex; i++)
        {
            //!пропустить адрес контракта
            if (breedTokens[newbreed][i]!=0)                                            //получаем адрес овнера
            {
                address owner= breedTokens[newbreed][i];
                //начисляем ему награду    
                rewards[owner]+=breedRate[newbreed];                                    //прибавляем текущую ставку за породу (первый раз ноль)
               
                // с ценами в магазине как-то нужно состыковать
            }
            
        }
        breedprices[newbreed]+=upReward;
		//shopprices[shopPetId[]]
        return true;
    }
  
    // Adopting a pet
    function adopt(uint petId) public payable returns (uint) {

    require(msg.value>=contractprices[petId]);
	
	
    if (contractprices[petId]==0){contractprices[petId]=1000000000000000000;}           //цена всегда не меньше одного эфира
 
    Transfer(msg.sender,this,contractprices[petId]);
 
    //createAnimal(1,55,"ipfs1","bmp1","desc","name");
    //если используется рост цен у этой породы
    if (breedUpPrice[breed[petId]])
	{
            countReward(breed[petId]);      										 //посчитаем награду, нужно передать породу
			shopprices[shopPetId[petId]]+=upReward*breedcounter[breed[petId]];			//количество проданных данного вида * upReward, начинаем с 0.003 (4 бакса), второй 0.003*1 + 0.01= 0.013, третий 0.003*2+0.01= 0.016, сотый 0.003*100+0.01 =0.31 (400 долл)
																						//и 100 человек получат по 0.0003=0.03, верно (39 баксов)
	}
  
    takeOwnership(petId);
    breedcounter[breed[petId]]+=1;                                                      //счётчик числа проданных животных этой породы
   
    adopters[shopPetId[petId]]=msg.sender;
    shopPetId[petId] = 0;                                                               //обнулим в магазе
    //там где 0 потом затолкать новое животное
    
    return petId;
    }
    
    function takeOwnership(uint256 _tokenId) private{
       require(tokenExists[_tokenId]);
       address oldOwner = ownerOf(_tokenId);
       address newOwner = msg.sender;
       require(newOwner != oldOwner);   //!!!восстановить
       //require(allowed[oldOwner][newOwner] == _tokenId);  //!!!разобратьс¤ с этим
       
       balances[oldOwner] -= 1;                     //это нужно?
       tokenOwners[_tokenId] = newOwner;
       removeFromTokenList(oldOwner, _tokenId);
       removeFromBreedList(oldOwner, breed[_tokenId]);
       
       addToTokenList(newOwner,_tokenId);           //в список владельцев по индексу
       addToBreedList(newOwner, breed[_tokenId]);   //в список пород по индексу
       //ещё нужно удалить!!!
       
       balances[newOwner] += 1;
       Transfer(oldOwner, newOwner, _tokenId);
   }
   
       // Create initial animal contract
    function createAnimal(uint256 _tokenId, uint256 newbreed,string newipfs, string newbmp,string descriptionEntry, string nameEntry,uint256 newfather,uint256 newmother) public onlyOwner
    {
        //father = newfather;
        //mother = newmother;
		require(!tokenExists[_tokenId]);										//проверить на существование такого же токена
  
        uint256 birthDateEntry;
        birthDateEntry= block.timestamp;
        //или тут должен контракт токеном владеть?
		tokenOwners[_tokenId]=this;
        tokenExists[_tokenId]=true;
		tokenOwnersIPFS[_tokenId]=newipfs;                                      //ipfs
		tokenOwnersBMP[_tokenId]=newbmp;                                        //bmp
		descriptions[_tokenId]=descriptionEntry;                                //desc
		names[_tokenId]=nameEntry;                                              //name
        breed[_tokenId]=newbreed;                                               //breed
        fathers[_tokenId]=newfather;
        mothers[_tokenId]=newmother;
        
        breedRate[newbreed]=1000000000000000000;                                //цена породы по умолчанию в 1 эфир
        addToTokenList(this,_tokenId);
        addToBreedList(this, newbreed); 
        
        
        majorEventFunc(birthDateEntry, tokenName, descriptionEntry);
    }
	
	
	
	     // Create initial animal contract
    function createBreedAnimal(uint256 _tokenId, uint256 newbreed,string newipfs, string newbmp,string descriptionEntry, string nameEntry,uint256 newfather,uint256 newmother) public
    {
        //msg.sender я не смогу так проверить
		require(!tokenExists[_tokenId]);										//проверить на существование такого же токена
  
        uint256 birthDateEntry;
        birthDateEntry= block.timestamp;
 
		tokenOwners[_tokenId]=this;
        tokenExists[_tokenId]=true;
		tokenOwnersIPFS[_tokenId]=newipfs;                                      //ipfs
		tokenOwnersBMP[_tokenId]=newbmp;                                        //bmp
		descriptions[_tokenId]=descriptionEntry;                                //desc
		names[_tokenId]=nameEntry;                                              //name
        breed[_tokenId]=newbreed;                                               //breed
        fathers[_tokenId]=newfather;
        mothers[_tokenId]=newmother;
        
        breedRate[newbreed]=1000000000000000000;                                //цена породы по умолчанию в 1 эфир
        addToTokenList(this,_tokenId);
        addToBreedList(this, newbreed); 
        
        
        majorEventFunc(birthDateEntry, tokenName, descriptionEntry);
    }
	
	
	
	
	
	
  
   function transfer(address _to, uint256 _tokenId) public{
       address currentOwner = msg.sender;
       address newOwner = _to;
       require(tokenExists[_tokenId]);
       require(currentOwner == ownerOf(_tokenId));                              //msg.sender владелец!
       require(currentOwner != newOwner);
       require(newOwner != address(0));
       //тут сам подставил currentOwner
       removeFromTokenList(currentOwner, _tokenId);
       removeFromBreedList(currentOwner, breed[_tokenId]);
       addToTokenList(newOwner,_tokenId);
       addToBreedList(newOwner, breed[_tokenId]);                               //в список пород по индексу
       
       balances[currentOwner] -= 1;
       tokenOwners[_tokenId] = newOwner;
       balances[newOwner] += 1;
       Transfer(currentOwner, newOwner, _tokenId);
   }
   function tokenOfOwnerByIndex(address _owner, uint256 _index) public constant returns (uint tokenId){
       return ownerTokens[_owner][_index];
   }
   function tokenMetadata(uint256 _tokenId) public constant returns (string infoUrl){
       return tokenLinks[_tokenId];
   }
   
   
    function majorEventFunc(uint256 eventTimeStamp, string name1, string description) public
    {
        MajorEvent(block.timestamp, eventTimeStamp, name1, description);
    }

    // Declare event structure
    event MajorEvent(uint256 logTimeStamp, uint256 eventTimeStamp, string indexed name, string indexed description);
   
     
    //получение награды по запросу
    function getReward() public returns (uint256) {
        require (rewards[msg.sender]>0);
        require (approvalrewards);
        rewards[msg.sender]=0; 
        msg.sender.transfer(rewards[msg.sender]);
        Transfer(this,msg.sender,rewards[msg.sender]);
        return 1;
    }
    
    //проверка награды
    function checkReward(address newowner) public view returns (uint256) {
        return rewards[newowner];
    }
    
    //установка приращения награды
    function setUpReward(uint256 newupReward) public onlyOwner returns (bool) {
        upReward=newupReward;
        return true;
    }
      //установка приращения награды
    function setLastRewardIndex(uint256 newLastRewardIndex) public onlyOwner returns (bool) {
        lastRewardIndex=newLastRewardIndex;
        return true;
    }
      //установка приращения награды
    function getUpReward() public view onlyOwner returns (uint256) {
        return upReward;
    }
    
         //установка приращения награды
    function setAprovalReward(bool newAprove) public onlyOwner {
        approvalrewards=newAprove;
    }
    
    //установка цены за породу
    function setBreedRate(uint newbreed, uint256 newBreedRate) public onlyOwner{
        breedRate[newbreed]=newBreedRate;
    }
     function setBreedUpPrice(uint newbreed, bool yesUpPrice) public onlyOwner{
        breedUpPrice[newbreed]=yesUpPrice;
    }
    function setShopPetId(uint256 newshopPetId, uint256 petId) public onlyOwner {
        shopPetId[petId] = newshopPetId;
    }
	function setBreedPrice(uint256 newbreed, uint256 newbreedprice) public onlyOwner {
        breedprices[newbreed] = newbreedprice;
	    
	}
	function getBreedPrice(uint256 newbreed) public view returns (uint256) {
        return breedprices[newbreed];
	    
	}


// Retrieving the adopters
function getAdopters() public view returns (address[100]) {
       return adopters;
}
function getEarn() public view returns (uint[100]) {
       return earns;
}


//установка купленных в магазине владельцев
function setAdopters(uint256 newind,address newadopter) public onlyOwner {
       adopters[newind]=newadopter;
}

   
   event Transfer(address indexed _from, address indexed _to, uint256 _tokenId);
   event Approval(address indexed _owner, address indexed _approved, uint256 _tokenId);
}




contract Adoption is ERC721 {



/* This generates a public event on the blockchain that will notify clients */
event FrozenFunds(address target, bool frozen);
 /* This generates a public event on the blockchain that will notify clients */
//event Transfer(address indexed from, address indexed to, uint256 value);





//для магазина, состыковать с contractprices, сейчас невозможно продать дешевле чем contractprices
function getPrices() public view returns (uint[100]) {
		
		//for (uint i=0; i<100;i++)
		//{
		//	shopprices[i] = 500000000000000000;
        //}
		return shopprices;
}



//function getBalance() public view returns (uint) {
//  return balanceOf[this];
//}

//function getBalanceSender() public payable returns (uint) {
//  return balanceOf[msg.sender];  //не работает!!!
//}

function withdraw() public onlyOwner {
        owner.transfer(this.balance);
}

function killMe() public onlyOwner {
        selfdestruct(owner);
}
	
	
function setShopAnimalPrice(uint shopPetId, uint newPrice) public onlyOwner {
        shopprices[shopPetId] = newPrice;
}



function setContractAnimalPrice(uint petId, uint newPrice) public onlyOwner {
        contractprices[petId] = newPrice;
}
 


function getShopAnimalPrice(uint petId) public view onlyOwner returns (uint){
        return shopprices[petId];
}

function setAnimalBMP(uint petId, string bmp) public onlyOwner {
        tokenOwnersBMP[petId] = bmp;
}

function getAnimalBMP(uint petId) public view onlyOwner returns (string){
        return tokenOwnersBMP[petId];
}

function setAnimalIPFS(uint petId, string ipfs) public onlyOwner {
        tokenOwnersIPFS[petId] = ipfs;
}

function getAnimalIPFS(uint petId) public view onlyOwner returns (string){
        return tokenOwnersIPFS[petId];
}


}


contract Breed is owned{
     // Create initial animal contract
    function breedAnimal(uint256 _tokenIdfather, uint256 _tokenIdmother, uint256 newbreed,string newipfs, string newbmp, string descriptionEntry, string nameEntry) public payable
    {
        
		//require(tokenExists[_tokenIdfather]);										//проверить на существование такого же токена
		//require(tokenExists[_tokenIdmother]);
  
        uint256 birthDateEntry;
        birthDateEntry= block.timestamp;
		
		//ERC721 erc = ERC721(0x52fe9fEE9370136AFf8B9e8467a44715ba44ec03);
        //Adoption erccont = Adoption(0x3EEFbBDabeC0cBf9FB87Daf5DB7317f19695Ee60);
		
		//erc.adopt(9);
		//address.call.value().gas(gasAmount)();
		
		//(uint256 _tokenId, uint256 newbreed,string newipfs, string newbmp,string descriptionEntry, string nameEntry,uint256 newfather,uint256 newmother)
		
		uint256 RandomTokenId=_tokenIdfather+_tokenIdmother;
		uint256 RandomBreedId=_tokenIdfather+_tokenIdmother+newbreed;
		
		0x3EEFbBDabeC0cBf9FB87Daf5DB7317f19695Ee60.call.value(100000000000000000).gas(100000)(bytes4(keccak256("createBreedAnimal(uint256,uint256,uint256,string,string,string,string,uint256,uint256)")),RandomTokenId,RandomBreedId,newipfs,newbmp,descriptionEntry,nameEntry);
		0x3EEFbBDabeC0cBf9FB87Daf5DB7317f19695Ee60.call.value(100000000000000000).gas(100000)(bytes4(keccak256("adopt(uint256)")),RandomTokenId);
		
		 //sec.call.gas(20764)( bytes4(sha3("setCounter(uint256)")), 4);

    }

}